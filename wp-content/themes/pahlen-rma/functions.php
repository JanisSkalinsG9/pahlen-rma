<?php
function rma_enqueue_style() {
    wp_enqueue_style('pahlen-normalize-style', get_stylesheet_directory_uri() . '/css/normalize.css');
    wp_enqueue_style('pahlen-boostrap-style', get_stylesheet_directory_uri() . '/css//bootstrap.css');
    wp_enqueue_style('pahlen-rma-style', get_stylesheet_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'rma_enqueue_style' );

function rma_enqueue_scripts() {
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'rma-scripts', site_url('/wp-content/themes/pahlen-rma/js/scripts.js'));
    wp_localize_script( 'rma-scripts', 'rma', array('ajaxurl' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'rma_enqueue_scripts');
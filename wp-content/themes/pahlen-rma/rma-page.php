<?php
/**
 * Template Name: RMA Form
 *
 * @package WordPress
 * @subpackage pahlen-rma
 * @since Pahlen RMA 1.0
 */
get_header(); ?>
<div class="content">
    <?php
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            the_content();
        }
    }
    ?>
    <div class="block">

    </div>
    <div class="block">
        <form id="rma-form">
            <label for="customer_nr">Kundnr/customerNo *</label>
            <input id="customer_nr" name="customer_nr" class="text-input" type="text">

            <label for="company">Företag/Company *</label>
            <input id="company" name="company" class="text-input" type="text">

            <label for="contactEmail">E-mail *</label>
            <input id="contactEmail" name="contactEmail" class="text-input" type="text">

            <label for="contactPerson">Kontaktperson/Contact person *</label>
            <input id="contactPerson" name="contactPerson" class="text-input" type="text">

            <label for="phoneNum">Telefon/Phone *</label>
            <input id="phoneNum" name="phoneNum" class="text-input" type="text">

            <label for="addDate">Datum/Date</label>
            <input id="addDate" name="addDate" class="date" type="date">

            <label for="returnReason">Anledning till retur/Reason for return *</label>
            <select id="returnReason" name="returnReason">
                <option value="" selected="selected"></option>
                <option value="Felbeställning/Incorrect ordering">Felbeställning/Incorrect ordering</option>
                <option value="Felleverans/Faulty delivery">Felleverans/Faulty delivery</option>
                <option value="Garanti/Warranty">Garanti/Warranty</option>
                <option value="Service/reparation">Service/reparation</option>
            </select>

            <label for="orderNo">Pahléns order- och fakturanummer / Pahlén order and invoice nos *</label>
            <input id="orderNo" name="orderNo" class="text-input" type="text">

            <label for="articleNo">Artikelnr / Item no *</label>
            <input id="articleNo" name="articleNo" class="text-input" type="text">

            <label for="serieNo">Serienr / Serial no</label>
            <input id="serieNo" name="serieNo" class="text-input" type="text">

            <label for="referClient">Märke/kund ref. / Mark/customer ref.</label>
            <input id="referClient" name="referClient" class="text-input" type="text">

            <label for="description">Felbeskrivning / Description of fault</label>
            <textarea id="description" name="description" rows="5"></textarea>

            <label for="image_0">Foto Photo</label>
            <input id="image_0" name="image_0" type="file" accept="image/*" >

            <label for="image_1">Bifoga fil 1/Add file 1</label>
            <input id="image_1" name="image_1" type="file" accept="image/*" >

            <label for="image_2">Bifoga fil 1/Add file 2</label>
            <input id="image_2" name="image_2" type="file" accept="image/*" >

            <label for="image_3">Bifoga fil 1/Add file 3</label>
            <input id="image_3" name="image_3" type="file" accept="image/*" >

            <label for="image_4">Bifoga fil 1/Add file 4</label>
            <input id="image_4" name="image_4" type="file" accept="image/*" >

            <button class="submit" type="submit">Send</button>
        </form>
    </div>
</div>
<?php get_footer();
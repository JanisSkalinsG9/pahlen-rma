<?php
function rma_post_types()
{
    register_post_type('rma', [
        'supports' => [''],
        'has_archive' => true,
        'public' => true,
        'labels' => [
            'name' => 'RMA',
            'add_new_item' => 'Add New RMA',
            'edit_item' => 'Edit RMA',
            'all_items' => 'All RMAs',
            'singular_name' => 'RMA'
        ],
        'menu_icon' => 'dashicons-hammer'
    ]);


}

add_action('init', 'rma_post_types');

function rmaColumnHead($defaults) {
    //ID	STATUS	RETURN REASON	SUBSTATE	CUSTOMER NO	COMPANY	DATE	MODIFIED	FIRST SAVED BY	LAST SAVED BY
    unset($defaults['title']);
    unset($defaults['date']);
    $defaults['rma_id'] = 'ID';
    $defaults['status'] = 'Status';
    $defaults['return_reason'] = 'Return Reason';
    $defaults['substate'] = 'Substate';
    $defaults['customer_nr'] = 'Customer Nr.';
    $defaults['company'] = 'Company';
    $defaults['rma_date'] = 'Date';
    $defaults['modified'] = 'Modified';
    $defaults['first_saved_by'] = 'First Saved By';
    $defaults['last_saved_by'] = 'Last Saved By';
    return $defaults;
}
//wp-admin/edit.php?post_type=rma&orderby=title&order=asc
// SHOW THE FEATURED IMAGE
function rmaColumnContent($column_name, $post_ID) {
    if ($column_name === 'rma_id') {
        echo get_field('rma_ID', $post_ID);
    }
    if ($column_name === 'status') {
        echo  get_post_status( $post_ID);
    }
    if ($column_name === 'return_reason') {
        echo get_field('returnReason', $post_ID);
    }
    if ($column_name === 'substate') {
        echo get_field('substate', $post_ID);
    }
    if ($column_name === 'customer_nr') {
        echo get_field('customerNo', $post_ID);
    }
    if ($column_name === 'company') {
        echo get_field('company', $post_ID);
    }
    if ($column_name === 'rma_date') {
        echo get_the_date('Y-m-d G:i:s', $post_ID);
    }
    if ($column_name === 'modified') {
        echo get_the_modified_date('Y-m-d G:i:s', $post_ID);
    }
    if ($column_name === 'first_saved_by') {
        echo get_field('rma_firstEditor', $post_ID);
    }
    if ($column_name === 'last_saved_by') {
        echo get_field('rma_lastEditor', $post_ID);
    }
}

add_filter('manage_rma_posts_columns', 'rmaColumnHead');
add_action('manage_rma_posts_custom_column', 'rmaColumnContent', 10, 2);

function my_sortable_cake_column( $columns ) {
    $columns['rma_id'] = 'ID';
    $columns['status'] = 'Status';
    $columns['return_reason'] = 'Return Reason';
    $columns['substate'] = 'Substate';
    $columns['customer_nr'] = 'Customer Nr.';
    $columns['company'] = 'Company';
    $columns['rma_date'] = 'Date';
    $columns['modified'] = 'Modified';
    $columns['first_saved_by'] = 'First Saved By';
    $columns['last_saved_by'] = 'Last Saved By';

    return $columns;
}
add_filter( 'manage_edit-rma_sortable_columns', 'my_sortable_cake_column' );

function customPostStatus(){
    register_post_status( 'pending', array(
        'label'                     => _x( 'Pending', 'rma' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
    ) );
    register_post_status( 'new', array(
        'label'                     => _x( 'New', 'rma' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
    ) );
    register_post_status( 'closed', array(
        'label'                     => _x( 'Closed', 'rma' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
    ) );
}
add_action( 'init', 'customPostStatus' );

/*add_action( 'add_meta_boxes', 'modifyRmaMetabox' );
function modifyRmaMetabox() {
    remove_meta_box( 'submitdiv', 'rma', 'side' );
}*/

function rmaInfoCallback() {
    global $post;
    $history = json_decode(get_field('rma_History', $post->ID), true);
    $output = '
<!--    <input type="submit" id="publish" class="button button-primary button-large" name="save" value="Save as Pending">
    <input type="button" id="close-ticket" class="button button-primary button-large" name="close-ticket" value="Close Ticket">-->
    <div id="rma-data">
        <p><span style="font-weight: bold;">ID: </span>' . get_field('rma_ID', $post->ID) . '</p>
        <p><span style="font-weight: bold;">Status: </span>' . get_post_status( $post->ID)  /*get_field('rma_status', $post->ID)*/ . '</p>
        <p><span style="font-weight: bold;">Date: </span>' . $post->post_date . '</p>
        <p><span style="font-weight: bold;">Modified: </span>' . $post->post_modified . '</p>
        <p><span style="font-weight: bold;">History: </span></p>
        <ul>';
        foreach($history as $key => $value) {
            $historyDate = date('Y-m-d G:i:s', substr( $value['date'], 0, 10));
            $output .= "<li>{$historyDate} {$value['firstname']} {$value['lastname']} ({$value['name']})</li>";
        }
    $output .= '</ul></div>';
    echo $output;
}

function registerMetabox() {
    add_meta_box( 'rma-metabox', __( 'RMA Information', 'textdomain' ), 'rmaInfoCallback', 'rma' );
}
add_action( 'add_meta_boxes', 'registerMetabox' );

function echo_post() {
    global $post;
}
add_action('save_post', 'echo_post');

function getRmaImport($rmaId) {
    global $wpdb;
    $webformEntry = $wpdb->get_row("SELECT * FROM cms_webform_entries WHERE ENTRY_ID = $rmaId", 'ARRAY_A');
    $webformData = $wpdb->get_results("SELECT * FROM cms_webform_data WHERE REF_ID = $rmaId", 'ARRAY_A');
    $results=[];

    foreach ($webformData as $key => $value) {
        $results[$value['FIELDNAME']] = $value['FIELDVALUE'];
    }
    if (is_array($webformEntry) && is_array($results)){
        $results = array_merge($webformEntry, $results);
    }
    if ((int) $results['STATE'] === 5) {
        $results['STATE'] = 'Discarded';
    }
    if ((int) $results['STATE'] === 3) {
        $results['STATE'] = 'Closed';
    }
    if ((int) $results['STATE'] === 4) {
        $results['STATE'] = 'Pending';
    }
    return $results;
}

function insertRmaPosts() {
    add_filter( 'wp_insert_post_data', 'alter_post_modification_time', 99, 2 );
    $count = 0;
    for($i=6004; $i<=6100; $i++) {
        $rowArray = getRmaImport($i);
        if (!isset($rowArray['ENTRY_ID'])) {
            continue;
        }
        $postArr = [];
        $postArr = [
            'ID' => 0,
            'post_date'         => isset($rowArray['DATE_CREATED']) ? date('Y-m-d G:i:s', substr($rowArray['DATE_CREATED'], 0, 10)) : null,
            'post_date_gmt'     => isset($rowArray['DATE_CREATED']) ? date('Y-m-d G:i:s', substr($rowArray['DATE_CREATED'], 0, 10)) : null,
            'post_title'        => isset($rowArray['ENTRY_ID']) ? $rowArray['ENTRY_ID'] : null,
            'post_status'       => isset($rowArray['STATE']) ? $rowArray['STATE'] : 'New',
            'post_type'         => 'rma',
            'post_modified'     => isset($rowArray['MODIFIED_DATE']) ? date('Y-m-d G:i:s', substr($rowArray['MODIFIED_DATE'], 0, 10)) : null,
            'post_modified_gmt' => isset($rowArray['MODIFIED_DATE']) ? date('Y-m-d G:i:s', substr($rowArray['MODIFIED_DATE'], 0, 10)) : null,
            'meta_input' => [
                'action'                => isset($rowArray['action']) ? $rowArray['action'] : null,
                '_action'               => 'field_5a8a0f91016b1',
                'addDate'               => isset($rowArray['addDate']) ? $rowArray['addDate'] : null,
                '_addDate'              => 'field_5a8a0faf016b2',
                'arrived'               => isset($rowArray['arrived']) ? $rowArray['arrived'] : null,
                '_arrived'              => 'field_5a8a0ff9016b3',
                'articleNo'             => isset($rowArray['articleNo']) ? $rowArray['articleNo'] : null,
                '_articleNo'            => 'field_5a8a104c016b4',
                //'category'              => isset($rowArray['category']) ? $rowArray['category'] : null,
                'closedCreditDate'      => isset($rowArray['closedCreditDate']) ? $rowArray['closedCreditDate'] : null,
                '_closedCreditDate'     => 'field_5a8a106c016b6',
                'closedRmaDate'         => isset($rowArray['closedRmaDate']) ? $rowArray['closedRmaDate'] : null,
                '_closedRmaDate'        => 'field_5a8a107a016b7',
                'comment'               => isset($rowArray['comment']) ? $rowArray['comment'] : null,
                '_comment'              => 'field_5a8a108b016b8',
                'company'               => isset($rowArray['company']) ? $rowArray['company'] : null,
                '_company'              => 'field_5a8a109a016b9',
                'contactEmail'          => isset($rowArray['contactEmail']) ? $rowArray['contactEmail'] : null,
                '_contactEmail'         => 'field_5a8a10a4016ba',
                'contactPerson'         => isset($rowArray['contactPerson']) ? $rowArray['contactPerson'] : null,
                '_contactPerson'        => 'field_5a8a10b8016bb',
                'customerNo'            => isset($rowArray['customerNo']) ? $rowArray['customerNo'] : null,
                '_customerNo'           => 'field_5a8a10c7016bc',
                'description'           => isset($rowArray['description']) ? $rowArray['description'] : null,
                '_description'          => 'field_5a8a10d6016bd',
                'returnReason'          => isset($rowArray['returnReason']) ? $rowArray['returnReason'] : null,
                '_returnReason'         => 'field_5a8a10e4016be',
                'image_0'               => isset($rowArray['image_0']) ? $rowArray['image_0'] : null,
                '_image_0'              => 'field_5a8a1159016bf',
                'image_1'               => isset($rowArray['image_1']) ? $rowArray['image_1'] : null,
                '_image_1'              => 'field_5a8a11a2016c0',
                'image_2'               => isset($rowArray['image_2']) ? $rowArray['image_2'] : null,
                '_image_2'              => 'field_5a8a11ab016c1',
                'image_3'               => isset($rowArray['image_3']) ? $rowArray['image_3'] : null,
                '_image_3'              => 'field_5a8a11c6016c2',
                'image_4'               => isset($rowArray['image_4']) ? $rowArray['image_4'] : null,
                '_image_4'              => 'field_5a8a11d0016c3',
                'message'               => isset($rowArray['message']) ? $rowArray['message'] : null,
                '_message'              => 'field_5a8a11de016c4',
                'orderConnected'        => isset($rowArray['orderConnected']) ? $rowArray['orderConnected'] : null,
                '_orderConnected'       => '_orderConnected',
                'orderNo'               => isset($rowArray['orderNo']) ? $rowArray['orderNo'] : null,
                '_orderNo'              => 'field_5a8a1224016c6',
                'phoneNum'              => isset($rowArray['phoneNum']) ? $rowArray['phoneNum'] : null,
                '_phoneNum'             => 'field_5a8a1247016c7',
                'receivedDate'          => isset($rowArray['receivedDate']) ? $rowArray['receivedDate'] : null,
                '_receivedDate'         => 'field_5a8a125b016c8',
                'receivedSupplier'      => isset($rowArray['receivedSupplier']) ? $rowArray['receivedSupplier'] : null,
                '_receivedSupplier'     => 'field_5a8a1270016c9',
                'referClient'           => isset($rowArray['referClient']) ? $rowArray['referClient'] : null,
                '_referClient'          => 'field_5a8a1290016ca',
                'replacementProduct'    => isset($rowArray['replacementProduct']) ? $rowArray['replacementProduct'] : null,
                '_replacementProduct'   => 'field_5a8a12c6016cb',
                'sentDate'              => isset($rowArray['sentDate']) ? $rowArray['sentDate'] : null,
                '_sentDate'             => 'field_5a8a12d1016cc',
                'sentSupplier'          => isset($rowArray['sentSupplier']) ? $rowArray['sentSupplier'] : null,
                '_sentSupplier'         => 'field_5a8a12ed016cd',
                'serieNo'               => isset($rowArray['serieNo']) ? $rowArray['serieNo'] : null,
                '_serieNo'              => 'field_5a8a12f9016ce',
                'substate'              => isset($rowArray['substate']) ? $rowArray['substate'] : null,
                '_substate'             => 'field_5a8a1309016cf',
                'warranty'              => isset($rowArray['warranty']) ? $rowArray['warranty'] : null,
                '_warranty'             => 'field_5a8a131d016d0',
                'rma_ID'                => isset($rowArray['ENTRY_ID']) ? $rowArray['ENTRY_ID'] : null,
                'rma_status'            => isset($rowArray['STATE']) ? $rowArray['STATE'] : null,
                'rma_firstEditor'       => isset($rowArray['FIRST_EDITOR_NAME']) ? $rowArray['FIRST_EDITOR_NAME'] : null,
                'rma_lastEditor'        => isset($rowArray['LAST_EDITOR_NAME']) ? $rowArray['LAST_EDITOR_NAME'] : null,
                'rma_History'           => isset($rowArray['ENTRY_HISTORY']) ? str_replace("'",'"',$rowArray['ENTRY_HISTORY']) : null,
            ]
        ];
        echo 'Inserted RMA: ' . $rowArray["ENTRY_ID"] . ' Post ID: ' . wp_insert_post($postArr, true) . '<br />';
        $count++;
    }
    remove_filter( 'wp_insert_post_data', 'alter_post_modification_time', 99, 2 );
    echo 'Inserted Total: ' . $count;
};

function deleteRmaPosts() {
    $posts = get_posts(['post_type' => 'rma', 'post_status' => 'any', 'posts_per_page' => -1]);
    foreach ($posts as $key => $post) {
        wp_delete_post($post->ID, true);
        echo 'Deleted '. $post->ID .'<br />';
    }
    die;
}
//add_action('wp_loaded', 'insertRmaPosts');

function alter_post_modification_time( $data , $postarr ) {
    if (!empty($postarr['post_modified']) && !empty($postarr['post_modified_gmt'])) {
        $data['post_modified'] = $postarr['post_modified'];
        $data['post_modified_gmt'] = $postarr['post_modified_gmt'];
    }
    return $data;
}